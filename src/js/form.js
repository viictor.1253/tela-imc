var bottunAdd = document.querySelector("#adicionar-paciente")

bottunAdd.addEventListener("click",function(event){
    event.preventDefault(); // nao carregar a pagina

    var form = document.querySelector("#form-add")

    var pacienteForm = dadosFormulario(form);
    
    var pacienteTr= criarTr(pacienteForm)

    var erros = validaPaciente(pacienteForm)
    
    if(erros.length > 0){
       exibeMensgemErros(erros)
        return;
    }
    
    var table = document.querySelector("#tabela-pacientes")

    table.appendChild(pacienteTr);

    form.reset()
    var ul = document.querySelector("#mensage-error");
    ul.innerHTML =""

});


function dadosFormulario(form){

    var paciente = {
        nome: form.nome.value,
        altura: form.altura.value,
        peso: form.peso.value,
        gordura: form.gordura.value,
        imc: calculaImc(form.peso.value, form.altura.value)
    }
     
    return paciente;
}

function criarTr(paciente){
    var pacienteTr = document.createElement('tr');
    pacienteTr.classList.add("paciente");

    pacienteTr.appendChild( montaTd(paciente.nome,"info-nome"))
    pacienteTr.appendChild(montaTd(paciente.peso,"info-peso"))
    pacienteTr.appendChild(montaTd(paciente.altura,"info-altura"))
    pacienteTr.appendChild( montaTd(paciente.gordura,"info-gordura"))
    pacienteTr.appendChild(montaTd(paciente.imc,"info-imc"))

    return pacienteTr
}


function montaTd(dado,classe){

    var td = document.createElement("td");
    td.textContent = dado;
    td.classList.add(classe);

    return td
}


function validaPaciente(paciente){
    var erros = [];

    if(paciente.nome.length == 0) erros.push("O nome não pode estar em branco");

    if(!validaPeso(paciente.peso)) erros.push("peso Invalido");

    if(!validaAltura(paciente.altura)) erros.push("altura invalida");

    if(paciente.gordura.length == 0) erros.push("a gordura não pode estar em branco");

    if(paciente.peso.length == 0) erros.push("o peso não pode estar em branco");

    if(paciente.altura.length == 0) erros.push("a altura não pode estar em branco");


    return erros;
}

function exibeMensgemErros(erros){
    var ul = document.querySelector("#mensage-error");
    ul.innerHTML = ""

    erros.forEach(erro => {

        var li = document.createElement("li")

        li.textContent = erro

        ul.appendChild(li)
        
    });
}